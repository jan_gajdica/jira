package com.cgi.bootcamp.jg.jira.persistence.entities;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Ticket {

    private Long id;
    private String name;
    private String email;
    private Long idPersonCreator;
    private Long idPersonAssigned;
    private Timestamp creationDatetime;
    private Timestamp ticketCloseDatetime;

    public Ticket() {
    }

    public Ticket(Long id, String name, String email, Long idPersonCreator, Long idPersonAssigned, Timestamp creationDatetime, Timestamp ticketCloseDatetime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDatetime = creationDatetime;
        this.ticketCloseDatetime = ticketCloseDatetime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public Timestamp getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(Timestamp creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    public Timestamp getTicketCloseDatetime() {
        return ticketCloseDatetime;
    }

    public void setTicketCloseDatetime(Timestamp ticketCloseDatetime) {
        this.ticketCloseDatetime = ticketCloseDatetime;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDatetime=" + creationDatetime +
                ", ticketCloseDatetime=" + ticketCloseDatetime +
                '}';
    }
}
