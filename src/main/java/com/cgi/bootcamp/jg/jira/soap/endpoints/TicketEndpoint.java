package com.cgi.bootcamp.jg.jira.soap.endpoints;

import com.cgi.bootcamp.jg.jira.service.TicketService;
import com.cgi.bootcamp.jg.jira.ws.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.datatype.DatatypeConfigurationException;

@Endpoint
public class TicketEndpoint {

    private static final String NAMESPACE_URI = "http://cgi.com/ws";

    private TicketService ticketService;

    @Autowired
    public TicketEndpoint(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketByIdRequest")
    @ResponsePayload
    public GetTicketByIdResponse getTicketById(@RequestPayload GetTicketByIdRequest request) throws DatatypeConfigurationException {
        GetTicketByIdResponse getTicketByIdResponse = new GetTicketByIdResponse();
        getTicketByIdResponse.setTicketDTO(ticketService.getTicketById(request.getId()));
        return getTicketByIdResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketByNameRequest")
    @ResponsePayload
    public GetTicketByNameResponse getTicketByName(@RequestPayload GetTicketByNameRequest request) throws DatatypeConfigurationException {
        GetTicketByNameResponse getTicketByNameResponse = new GetTicketByNameResponse();
        getTicketByNameResponse.setTicketDTO(ticketService.getTicketByName(request.getName()));
        return getTicketByNameResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "CreateTicketRequest")
    @ResponsePayload
    public CreateTicketResponse CreateTicket(@RequestPayload CreateTicketRequest request) {
        CreateTicketResponse createTicketResponse = new CreateTicketResponse();
        createTicketResponse.setId(ticketService.putTicket(request.getCreateTicketDTO()));
        return createTicketResponse;
    }
}
