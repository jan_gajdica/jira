package com.cgi.bootcamp.jg.jira.service;

import com.cgi.bootcamp.jg.jira.persistence.entities.Ticket;
import com.cgi.bootcamp.jg.jira.persistence.repository.TicketRepository;
import com.cgi.bootcamp.jg.jira.ws.CreateTicketDTO;
import com.cgi.bootcamp.jg.jira.ws.TicketDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Service
public class TicketService {

    private TicketRepository ticketRepository;

    @Autowired
    public TicketService(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    public TicketDTO getTicketById(Long id) throws DatatypeConfigurationException {
        return mapTicketToDTO(ticketRepository.getTicketById(id));
    }

    public TicketDTO getTicketByName(String name) throws DatatypeConfigurationException {
        return mapTicketToDTO(ticketRepository.getTicketByName(name));
    }

    public Long putTicket(CreateTicketDTO ticketDTO) {
        ticketRepository.saveTicket(mapDTOToTicket(ticketDTO));
        return ticketRepository.getTicketByName(ticketDTO.getName()).getId();
    }

    public static TicketDTO mapTicketToDTO(Ticket ticket) throws DatatypeConfigurationException {
        TicketDTO ticketDTO = new TicketDTO();
        ticketDTO.setId(ticket.getId());
        ticketDTO.setName(ticket.getName());
        ticketDTO.setEmail(ticket.getEmail());
        ticketDTO.setIdPersonCreator(ticket.getIdPersonCreator());
        ticketDTO.setIdPersonAssigned(ticket.getIdPersonAssigned());
        LocalDateTime ldt = ticket.getCreationDatetime().toLocalDateTime();
        XMLGregorianCalendar cal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        cal.setYear(ldt.getYear());
        cal.setMonth(ldt.getMonthValue());
        cal.setDay(ldt.getDayOfMonth());
        cal.setHour(ldt.getHour());
        cal.setMinute(ldt.getMinute());
        cal.setSecond(ldt.getSecond());
        ticketDTO.setCreationDatetime(cal);
        ldt = ticket.getTicketCloseDatetime().toLocalDateTime();
        cal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        cal.setYear(ldt.getYear());
        cal.setMonth(ldt.getMonthValue());
        cal.setDay(ldt.getDayOfMonth());
        cal.setHour(ldt.getHour());
        cal.setMinute(ldt.getMinute());
        cal.setSecond(ldt.getSecond());
        ticketDTO.setTicketCloseDatetime(cal);
        return ticketDTO;
    }

    public static Ticket mapDTOToTicket(CreateTicketDTO ticketDTO) {
        Ticket ticket = new Ticket();
        ticket.setName(ticketDTO.getName());
        ticket.setEmail(ticketDTO.getEmail());
        ticket.setIdPersonCreator(ticketDTO.getIdPersonCreator());
        ticket.setIdPersonAssigned(ticketDTO.getIdPersonAssigned());
        Timestamp ts = new Timestamp(ticketDTO.getCreationDatetime().toGregorianCalendar().getTimeInMillis());
        ticket.setCreationDatetime(ts);
        ts = new Timestamp(ticketDTO.getTicketCloseDatetime().toGregorianCalendar().getTimeInMillis());
        ticket.setTicketCloseDatetime(ts);
        return ticket;
    }
}
