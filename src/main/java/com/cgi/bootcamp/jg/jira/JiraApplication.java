package com.cgi.bootcamp.jg.jira;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JiraApplication {

    public static void main(String[] args) {

        SpringApplication.run(JiraApplication.class, args);

        //AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(JiraApplication.class);

        //TicketService ticketService = annotationConfigApplicationContext.getBean(TicketService.class);
        //TicketDTO ticket = ticketService.getTicketById(1L);
        //System.out.println(ticket);

        //ticket = ticketService.getTicketByName("Issue1");
        //System.out.println(ticket);

        //Ticket ticket = new Ticket(1L, "Issue1", "email@email.cz", 1L, 2L, Timestamp.valueOf("2020-02-14 13:38:30"), Timestamp.valueOf("2020-02-15 13:40:45"));
        //ticketService.putTicket(ticket);
    }

}
