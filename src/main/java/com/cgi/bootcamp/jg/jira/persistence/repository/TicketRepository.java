package com.cgi.bootcamp.jg.jira.persistence.repository;

import com.cgi.bootcamp.jg.jira.persistence.entities.Ticket;
import com.cgi.bootcamp.jg.jira.persistence.mappers.TicketMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class TicketRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TicketRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Ticket getTicketById(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE id=?", new Object[]{id}, new TicketMapper());
    }

    public Ticket getTicketByName(String name) {
        return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE name=?", new Object[]{name}, new TicketMapper());
    }

    public void saveTicket(Ticket ticket) {
        jdbcTemplate.update("INSERT INTO public.ticket(id, name, email, id_person_creator, id_person_assigned, creation_datetime, ticket_close_datetime) "
                + "VALUES (DEFAULT, '"
                + ticket.getName() + "', '"
                + ticket.getEmail() + "', '"
                + ticket.getIdPersonCreator().toString() + "', '"
                + ticket.getIdPersonAssigned().toString() + "', '"
                + ticket.getCreationDatetime().toString() + "', '"
                + ticket.getTicketCloseDatetime().toString() + "');");
    }
}
